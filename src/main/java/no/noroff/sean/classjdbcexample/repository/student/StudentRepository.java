package no.noroff.sean.classjdbcexample.repository.student;

import no.noroff.sean.classjdbcexample.models.Student;
import no.noroff.sean.classjdbcexample.repository.CrudRepository;

public interface StudentRepository extends CrudRepository<Student, Integer> {
}
