package no.noroff.sean.classjdbcexample.models;

public record Student (int id, String name) {
}
