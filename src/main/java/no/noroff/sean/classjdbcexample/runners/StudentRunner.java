package no.noroff.sean.classjdbcexample.runners;

import no.noroff.sean.classjdbcexample.models.Student;
import no.noroff.sean.classjdbcexample.repository.student.StudentRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class StudentRunner implements ApplicationRunner {
    private final StudentRepository studentRepository;

    public StudentRunner(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println(studentRepository.findById(2).name());
    }
}
