package no.noroff.sean.classjdbcexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClassJdbcExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClassJdbcExampleApplication.class, args);
    }

}
